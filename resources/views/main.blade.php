@extends('layouts.app')

@section('navbar')
    <router-link to='/posts' title="Posts"><span>Posts</span>
    </router-link> |
    <router-link to='/example' title="Example"><span>Example</span>
    </router-link>
@endsection
@section('content')
    <div class="container">
        <router-view />
    </div>
@endsection
