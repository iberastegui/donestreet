import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

let routes = [
    {
        path: "/posts",
        name: "posts",
        component: () => import("./views/Posts")
    },
    {
        path: "/example",
        name: "example",
        component: () => import("./views/Example")
    }
];
routes.push({
    path: "*",
    component: () => import("./views/Posts")
});

const router = new VueRouter({
    base: "/",
    mode: "history",
    linkActiveClass: "font-bold",
    routes
});

export default router;
